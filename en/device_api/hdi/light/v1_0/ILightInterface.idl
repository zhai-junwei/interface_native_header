/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Light
 * @{
 * 
 * @brief Provides common APIs for the light service to access the light driver.
 * 
 * After obtaining the light driver object or proxy, the service can call the APIs to obtain light information,
 * turn on or off lights, or set the light blinking mode based on the light type.
 * 
 * @since 3.1
 * @version 1.0
 */

/**
 * @file ILightInterface.idl
 * 
 * @brief Declares the common APIs of the light module. The APIs can be used to obtain the light type, turn on or off lights, and set the light brightness and blinking parameters.
 * 
 * @since 3.1
 * @version 1.0
 */

/**
 * @brief Defines the path for the package of the light module APIs.
 * 
 * @since 3.1
 * @version 1.0
 */
package ohos.hdi.light.v1_0;
import ohos.hdi.light.v1_0.LightTypes;

/**
 * @brief Provides APIs for performing basic light operations,
 * including obtaining light information, turning on or off lights, and
 * setting the light brightness and blinking parameters.
 * 
 * @since 3.1
 * @version 1.0
 */
interface ILightInterface {
    /**
      * @brief Obtains information about all types of lights in the system.
     * 
     * @param info Indicates the double pointer to the light information obtained.
     * 
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a negative value if the operation fails.
     * 
     * @since 3.1
     * @version 1.0
     */
    GetLightInfo([out] struct HdfLightInfo[] info);

    /**
     * @brief Turns on available lights of the specified type.
     * 
     * @param lightId Indicates the light type. For details, see {@link HdfLightId}.
     * @param effect Indicates the pointer to the light effect. If <b>lightbrightness</b> is <b>0</b>,
     * the default brightness set in the HCS will be used. For details, see {@link HdfLightEffect}.
     *
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns <b>-1</b> if the light type is not supported.
     * @return Returns <b>-2</b> if the blinking setting is not supported.
     * @return Returns <b>-3</b> if the brightness setting is not supported.
     *
     * @since 3.1
     * @version 1.0
     */
    TurnOnLight([in] int lightId, [in] struct HdfLightEffect effect);

     /**
     * @brief Turns on multiple physical lights based on the specified light type.
     * 
     * @param lightId Indicates the light type. For details, see {@link HdfLightId}.
     * @param colors Indicates the colors and brightness of the physical lights to turn on. For details, see {@link HdfLightColor}.
     * 
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a negative value if the operation fails.
     * 
     * @since 3.2
     * @version 1.0
     */
    TurnOnMultiLights([in] int lightId, [in] struct HdfLightColor[] colors);

    /**
     * @brief Turns off available lights of the specified type.
     * 
     * @param lightId Indicates the light type. For details, see {@link HdfLightId}.
     * 
     * @return Returns <b>0</b> if the operation is successful.
     * @return Returns a negative value if the operation fails.
     * 
     * @since 3.1
     * @version 1.0
     */
    TurnOffLight([in] int lightId);
}
/** @} */
