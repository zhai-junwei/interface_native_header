/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_SHADER_EFFECT_H
#define C_INCLUDE_DRAWING_SHADER_EFFECT_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module does not provide the pixel unit. The pixel unit to use is consistent with the application context
 * environment. In the ArkUI development environment, the default pixel unit vp is used.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_shader_effect.h
 *
 * @brief Declares the functions related to the shader effect in the drawing module.
 *
 * File to include: native_drawing/drawing_shader_effect.h
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the tile modes of the shader effect.
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_TileMode {
    /**
     * Replicates the edge color if the shader effect draws outside of its original boundary.
     */
    CLAMP,
    /**
     * Repeats the shader effect's image in both horizontal and vertical directions.
     */
    REPEAT,
    /**
     * Repeats the shader effect's image in both horizontal and vertical directions, alternating mirror images.
     */
    MIRROR,
    /**
     * Renders the shader effect's image only within the original boundary, and returns transparent black elsewhere.
     */
    DECAL,
} OH_Drawing_TileMode;

/**
 * @brief Creates an <b>OH_Drawing_ShaderEffect</b> object that generates a linear gradient between two points.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param startPt Start point.
 * @param endPt End point.
 * @param colors Colors to distribute.
 * @param pos Relative position of each color in the color array.
 * @param size Number of colors and positions.
 * @param OH_Drawing_TileMode Tile mode of the shader effect.
 * For details about the available options, see {@link OH_Drawing_TileMoe}.
 * @return Returns the pointer to the <b>OH_Drawing_ShaderEffect</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_ShaderEffect* OH_Drawing_ShaderEffectCreateLinearGradient(const OH_Drawing_Point* startPt,
    const OH_Drawing_Point* endPt, const uint32_t* colors, const float* pos, uint32_t size, OH_Drawing_TileMode);

/**
 * @brief Creates an <b>OH_Drawing_ShaderEffect</b> object that generates a radial gradient based on
 * the center and radius of a circle.
 * The radial gradient transitions colors from the center to the ending shape in a radial manner.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param centerPt Center of the circle.
 * @param radius Radius of the circle.
 * @param colors Colors to distribute.
 * @param pos Relative position of each color in the color array.
 * @param size Number of colors and positions.
 * @param OH_Drawing_TileMode Tile mode of the shader effect.
 * For details about the available options, see {@link OH_Drawing_TileMoe}.
 * @return Returns the pointer to the <b>OH_Drawing_ShaderEffect</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_ShaderEffect* OH_Drawing_ShaderEffectCreateRadialGradient(const OH_Drawing_Point* centerPt, float radius,
    const uint32_t* colors, const float* pos, uint32_t size, OH_Drawing_TileMode);

/**
 * @brief Creates an <b>OH_Drawing_ShaderEffect</b> object that generates a sweep gradient based on the center.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param centerPt Center of the circle.
 * @param colors Colors to distribute.
 * @param pos Relative position of each color in the color array.
 * @param size Number of colors and positions.
 * @param OH_Drawing_TileMode Tile mode of the shader effect.
 * For details about the available options, see {@link OH_Drawing_TileMoe}.
 * @return Returns the pointer to the <b>OH_Drawing_ShaderEffect</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_ShaderEffect* OH_Drawing_ShaderEffectCreateSweepGradient(const OH_Drawing_Point* centerPt,
    const uint32_t* colors, const float* pos, uint32_t size, OH_Drawing_TileMode);

/**
 * @brief Destroys an <b>OH_Drawing_ShaderEffect</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_ShaderEffect Pointer to an <b>OH_Drawing_ShaderEffect</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_ShaderEffectDestroy(OH_Drawing_ShaderEffect*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
