/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_FONT_H
#define C_INCLUDE_DRAWING_FONT_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。
 * 本模块不提供像素单位，和应用上下文环境保持一致。如果处于ArkUI开发环境中，采用框架默认像素单位vp。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_font.h
 *
 * @brief 文件中定义了与字体相关的功能函数。
 *
 * 引用文件"native_drawing/drawing_font.h"
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 用于创建一个字体对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 函数会返回一个指针，指针指向创建的字体对象。
 * @since 11
 * @version 1.0
 */
OH_Drawing_Font* OH_Drawing_FontCreate(void);

/**
 * @brief 用于给字体设置字形。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字体对象的指针。
 * @param OH_Drawing_Typeface 指向字形对象的指针。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetTypeface(OH_Drawing_Font*, OH_Drawing_Typeface*);

/**
 * @brief 获取字形对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字体对象{@link OH_Drawing_Font}的指针。
 * @return OH_Drawing_Typeface 函数返回一个指针，指向字形对象{@link OH_Drawing_Typeface}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Typeface* OH_Drawing_FontGetTypeface(OH_Drawing_Font*);

/**
 * @brief 用于给字体设置文字大小。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字体对象的指针。
 * @param textSize 字体大小。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetTextSize(OH_Drawing_Font*, float textSize);

/**
 * @brief 获取文本所表示的字符数量。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字体对象{@link OH_Drawing_Font}的指针。
 * @param text 文本存储首地址。
 * @param byteLength 文本长度，单位为字节。
 * @param encoding 文本编码类型{@link OH_Drawing_TextEncoding}。
 * @since 12
 * @version 1.0
 */
int OH_Drawing_FontCountText(OH_Drawing_Font*, const void* text, size_t byteLength,
    OH_Drawing_TextEncoding encoding);

/**
 * @brief 用于设置线性可缩放字体。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字体对象的指针。
 * @param isLinearText 真为使能线性可缩放字体，假为不使能。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetLinearText(OH_Drawing_Font*, bool isLinearText);

/**
 * @brief 用于给字体设置文本倾斜。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字体对象的指针。
 * @param skewX X轴相对于Y轴的倾斜度。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetTextSkewX(OH_Drawing_Font*, float skewX);

/**
 * @brief 用于设置增加描边宽度以近似粗体字体效果。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字体对象的指针。
 * @param isFakeBoldText 真为使能增加描边宽度，假为不使能。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetFakeBoldText(OH_Drawing_Font*, bool isFakeBoldText);

/**
 * @brief 用于销毁字体对象并回收该对象占有的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字体对象的指针。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontDestroy(OH_Drawing_Font*);

/**
 * @brief 定义字体度量信息的结构体。
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_Font_Metrics {
    /** 指示哪些度量是有效的 */
    uint32_t fFlags;
    /** 字符最高点到基线的最大距离 */
    float top;
    /** 字符最高点到基线的推荐距离 */
    float ascent;
    /** 字符最低点到基线的推荐距离 */
    float descent;
    /** 字符最低点到基线的最大距离 */
    float bottom;
    /** 行间距 */
    float leading;
    /** 平均字符宽度，如果未知则为零 */
    float avgCharWidth;
    /** 最大字符宽度，如果未知则为零 */
    float maxCharWidth;
    /** 任何字形边界框原点左侧的最大范围，通常为负值；不推荐使用可变字体 */
    float xMin;
    /** 任何字形边界框原点右侧的最大范围，通常为负值；不推荐使用可变字体 */
    float xMax;
    /** 小写字母的高度，如果未知则为零，通常为负数 */
    float xHeight;
    /** 大写字母的高度，如果未知则为零，通常为负数 */
    float capHeight;
    /** 下划线粗细 */
    float underlineThickness;
    /** 表示下划线的位置，即从基线到文字下方笔画顶部的垂直距离，通常为正值 */
    float underlinePosition;
    /** 删除线粗细 */
    float strikeoutThickness;
    /** 表示删除线的位置，即从基线到文字上方笔画底部的垂直距离，通常为负值 */
    float strikeoutPosition;
} OH_Drawing_Font_Metrics;

/**
 * @brief 获取字体度量信息。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字体对象{@link OH_Drawing_Font}的指针。
 * @param OH_Drawing_Font_Metrics 指向字体度量信息对象{@link OH_Drawing_Font_Metrics}的指针。
 * @return 函数返回一个浮点数变量，表示建议的行间距。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_FontGetMetrics(OH_Drawing_Font*, OH_Drawing_Font_Metrics*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
