/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Distributed Camera
 * @{
 *
 * @brief Distributed Camera模块接口定义。
 *
 * Distributed Camera模块包括对分布式相机设备的操作、流的操作和各种回调等，这部分接口与Camera一致。
 * 通过IDCameraProvider与Source SA通信交互，实现分布式功能。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IDCameraProviderCallback.idl
 *
 * @brief 声明分布式相机SA服务的回调。调用者需要实现回调。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Distributed Camera设备接口的包路径。
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.distributed_camera.v1_0;

import ohos.hdi.distributed_camera.v1_0.DCameraTypes;

/**
 * @brief 定义Distributed Camera设备功能回调操作。
 *
 * 对Distributed Camera设备执行创建通道，创建流，捕获图像和更新设置等操作。
 */
[callback] interface IDCameraProviderCallback {
    /**
     * @brief 在源设备和目的设备之间创建传输通道。打开并初始化分布式相机会话。
     *
     * @param dhBase 分布式相机设备基础信息。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DCamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    OpenSession([in] struct DHBase dhBase);

    /**
     * @brief 关闭分布式相机会话，并销毁源设备和目的设备之间的传输通道。
     *
     * @param dhBase 分布式相机设备基础信息。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DCamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    CloseSession([in] struct DHBase dhBase);

    /**
     * @brief 配置流。
     *
     * @param dhBase 分布式相机设备基础信息。
     * @param streamInfos 流信息列表，流信息定义在{@link DCStreamInfo}。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DCamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    ConfigureStreams([in] struct DHBase dhBase,[in] struct DCStreamInfo[] streamInfos);

    /**
     * @brief 释放流。
     *
     * @param dhBase 分布式相机设备基础信息。
     * @param streamIds 要释放的流ID列表。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DCamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    ReleaseStreams([in] struct DHBase dhBase,[in] int[] streamIds);

    /**
     * @brief 开始捕获图像。
     *
     * 本接口必须在调用{@link ConfigStreams}配置流之后调用。
     * 图像捕获有两种模式，分别是连续捕获和单次捕获。
     *
     * @param dhBase 分布式相机设备基础信息。
     * @param captureInfos 捕获请求的参数信息，具体信息查看{@link DCCaptureInfo}。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DCamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    StartCapture([in] struct DHBase dhBase,[in] struct DCCaptureInfo[] captureInfos);

    /**
     * @brief 停止捕获图像。
     *
     * @param dhBase 分布式相机设备基础信息。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DCamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    StopCapture([in] struct DHBase dhBase,[in] int[] streamIds);

    /**
     * @brief 更新设备控制参数。
     *
     * @param dhBase 分布式相机设备基础信息。
     *
     * @param settings 设置参数，包括sensor帧率，3A相关参数等。具体信息查看{@link DCameraSettings}。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DCamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    UpdateSettings([in] struct DHBase dhBase,[in] struct DCameraSettings[] settings);
}
