/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiActivityRecognition
 * @{
 *
 * @brief 提供订阅和获取用户行为的API
 *
 * MSDP（Multimodal Sensor Data Platform）可以获取行为识别驱动程序的对象或代理，然后调用该对象或代理提供的API，
 * 获取设备支持的行为类型，订阅或取消订阅不同的行为事件，获取当前的行为事件，以及获取设备缓存的行为事件。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IActivityChangedCallback.idl
 *
 * @brief 定义行为识别数据上报回调函数接口。
 *
 * 在行为识别用户订阅行为识别数据时需要注册这个回调函数接口的实例。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief 行为识别模块接口的包路径。
 *
 * @since 3.2
 */
package ohos.hdi.activity_recognition.v1_0;

import ohos.hdi.activity_recognition.v1_0.ActivityRecognitionTypes;

/**
 * @brief 定义上报行为事件的回调函数
 *
 * 用户在获得订阅的行为事件或获取设备缓存的行为事件前，需要先注册该回调函数。只有当订阅的行为发生时，行为数据才会通过回调函数进行上报。
 * 详情可参考{@link IActivityInterface}。
 *
 * @since 3.2
 */

[callback] interface IActivityChangedCallback {
    /**
     * @brief 定义上报行为事件的回调函数。
     *
     * 数据会通过该回调函数进行上报。
     *
     * @param event 上报的数据。详见{@link ActRecognitionEvent}定义。
     *
     * @return 如果回调函数上报数据成功，则返回0。
     * @return 如果回调函数上报数据成功，则返回负值。
     *
     * @since 3.2
     */
    OnActivityChanged([in] struct ActRecognitionEvent[] event);
}
/** @} */