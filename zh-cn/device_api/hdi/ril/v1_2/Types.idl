/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Ril
 * @{
 *
 * @brief Ril模块接口定义。
 *
 * Ril模块为上层电话服务提供相关调用接口，涉及电话、短信、彩信、网络搜索、SIM卡等功能接口及各种回调等。
 *
 * @since 4.1
 * @version 1.2
 */

/**
 * @file Types.idl
 *
 * @brief Ril模块HDI接口使用的数据类型。
 *
 * @since 4.1
 * @version 1.2
 */

package ohos.hdi.ril.v1_2;

import ohos.hdi.ril.v1_1.Types;

/**
 * @brief 随卡信息。
 *
 * @since 4.1
 * @version 1.2
 */
struct NcfgOperatorInfo {
    /**
     * 随卡匹配的运营商名称。
     */
    String operName;
    /**
     * 随卡匹配的运营商标识。
     */
    String operKey;
    /**
     * SIM卡状态。
     */
    int state;
    /**
     * 预留字段。
     */
    String reserve;
};
