/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Input
 * @{
 *
 * @brief Input模块驱动接口声明。
 *
 * 本模块为Input服务提供相关驱动接口，包括Input设备的打开和关闭、Input事件获取、设备信息查询、回调函数注册、特性状态控制等接口。
 *
 * @since 1.0
 * @version 1.0
 */

 /**
 * @file input_type.h
 *
 * @brief Input设备相关的类型定义，定义了Input设备驱动接口所使用的结构体及枚举类型。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef INPUT_TYPES_H
#define INPUT_TYPES_H

#include <stdint.h>
#include <stdbool.h>
#include <sys/time.h>

#ifdef __cplusplus
extern "C" {
#endif

/** Input设备数量的最大值 */
#define MAX_INPUT_DEV_NUM 32

/** 芯片信息长度 */
#define CHIP_INFO_LEN 10

/** 芯片名称长度 */
#define CHIP_NAME_LEN 10

/** 厂商名称长度 */
#define VENDOR_NAME_LEN 10

/** Input设备名称长度 */
#define DEV_NAME_LEN 64

/** 自测试结果长度 */
#define SELF_TEST_RESULT_LEN 20

/** Input设备节点服务名称 */
#define DEV_MANAGER_SERVICE_NAME "hdf_input_host"
#ifdef DIV_ROUND_UP
#undef DIV_ROUND_UP
#endif

/** 向上取整计算公式 */
#define DIV_ROUND_UP(nr, d) (((nr) + (d) - 1) / (d))

/** 一个字节所包含的比特数 */
#define BYTE_HAS_BITS 8

/** 比特与64位无符号整数的转换公式 */
#define BITS_TO_UINT64(count)    DIV_ROUND_UP(count, BYTE_HAS_BITS * sizeof(uint64_t))

/** Input设备发送力反馈命令的数量最大值 */
#define HDF_FF_CNT    (0x7f + 1)

/**
 * @brief 定义返回值类型。
 */
enum RetStatus {
    /** 成功 */
    INPUT_SUCCESS        = 0,
    
    /** 失败 */
    INPUT_FAILURE        = -1,

    /** 无效参数 */
    INPUT_INVALID_PARAM  = -2,

    /** 内存不足 */
    INPUT_NOMEM          = -3,

    /** 空指针 */
    INPUT_NULL_PTR       = -4,

    /** 执行超时 */
    INPUT_TIMEOUT        = -5,

    /** 特性不支持 */
    INPUT_UNSUPPORTED    = -6,
};

/**
 * @brief 定义Input设备类型。
 */
enum InputDevType {
    /** 触摸屏 */
    INDEV_TYPE_TOUCH,

    /** 物理按键 */
    INDEV_TYPE_KEY,

    /** 键盘 */
    INDEV_TYPE_KEYBOARD,

    /** 鼠标 */
    INDEV_TYPE_MOUSE,

    /** 虚拟按键 */
    INDEV_TYPE_BUTTON,

    /** 表冠 */
    INDEV_TYPE_CROWN,

    /** 自定义编码的特定功能或者事件 */
    INDEV_TYPE_ENCODER,

    /** 未知输入设备类型 */
    INDEV_TYPE_UNKNOWN,
};

/**
 * @brief 定义电源状态。
 */
enum PowerStatus {
    /** 正常唤醒 */
    INPUT_RESUME,

    /** 休眠下电模式 */
    INPUT_SUSPEND,

    /** 休眠低功耗模式 */
    INPUT_LOW_POWER,

    /** 未知电源状态 */
    INPUT_POWER_STATUS_UNKNOWN,
};

/**
 * @brief 定义容值测试类型。
 */
enum CapacitanceTest {
    /** 基础容值测试 */
    BASE_TEST,

    /** 全量容值自检测试 */
    FULL_TEST,

    /** MMI容值测试 */
    MMI_TEST,

    /** 老化容值测试 */
    RUNNING_TEST,

    /** 未知的测试类型 */
    TEST_TYPE_UNKNOWN,
};

/**
 * @brief Input事件数据包结构。
 */
typedef struct {
    /** 输入事件的属性 */
    uint32_t type;

    /** 输入事件的特定编码项 */
    uint32_t code;

    /** 输入事件编码项对应的值 */
    int32_t value;

    /** 输入事件对应的时间戳 */
    uint64_t timestamp;
} InputEventPackage;

/**
 * @brief 热插拔事件数据包结构。
 */
typedef struct {
    /** 设备索引 */
    uint32_t devIndex;

    /** 设备类型 */
    uint32_t devType;

    /** 设备状态。
     *
     * 1: 离线。
     * 0: 在线。
     *
     */
    uint32_t status;
} InputHotPlugEvent;

/**
 * @brief Input设备描述信息。
 */
typedef struct {
    /** 设备索引 */
    uint32_t devIndex;

    /** 设备类型 */
    uint32_t devType;
} InputDevDesc;

/**
 * @brief 此结构体定义了输入事件回调函数并提供给Input服务使用。
 */
typedef struct {
    /**
     * @brief 输入事件数据上报的回调函数。
     *
     * @param eventData 输入参数，驱动上报的Input事件数据。
     * @param count 输入参数，Input事件数据包的个数。
     * @param devIndex 输入参数，Input设备索引，用于标志多个Input设备，取值从0开始，最多支持32个设备。
     *
     * @since 1.0
     * @version 1.0
     */
    void (*EventPkgCallback)(const InputEventPackage **pkgs, uint32_t count, uint32_t devIndex);
} InputEventCb;

/**
 * @brief 此结构体定义了热插拔事件上报回调函数并提供给Input服务使用。
 */
typedef struct {
    /**
     * @brief 热插拔事件上报的回调函数。
     *
     * @param event 输入参数，上报的热插拔事件数据。
     *
     * @since 1.0
     * @version 1.0
     */
    void (*HotPlugCallback)(const InputHotPlugEvent* event);
} InputHostCb;

/**
 * @brief Input设备的能力属性，存储支持事件的位图。
 *
 * 用位的方式来表示该Input设备能够上报的事件类型。
 *
 */
typedef struct {
    /** 设备属性 */
    uint64_t devProp[BITS_TO_UINT64(INPUT_PROP_CNT)];

    /** 用于记录支持的事件类型的位图 */
    uint64_t eventType[BITS_TO_UINT64(EV_CNT)];

    /** 记录支持的绝对坐标的位图 */
    uint64_t absCode[BITS_TO_UINT64(ABS_CNT)];

    /** 记录支持的相对坐标的位图 */
    uint64_t relCode[BITS_TO_UINT64(REL_CNT)];

    /** 记录支持的按键值的位图 */
    uint64_t keyCode[BITS_TO_UINT64(KEY_CNT)];

    /** 记录设备支持的指示灯的位图 */
    uint64_t ledCode[BITS_TO_UINT64(LED_CNT)];

    /** 记录设备支持的其他功能的位图 */
    uint64_t miscCode[BITS_TO_UINT64(MSC_CNT)];

    /** 记录设备支持的声音或警报的位图 */
    uint64_t soundCode[BITS_TO_UINT64(SND_CNT)];

    /** 记录设备支持的作用力功能的位图 */
    uint64_t forceCode[BITS_TO_UINT64(HDF_FF_CNT)];

    /** 记录设备支持的开关功能的位图 */
    uint64_t switchCode[BITS_TO_UINT64(SW_CNT)];

    /** 按键状态的位图 */
    uint64_t keyType[BITS_TO_UINT64(KEY_CNT)];

    /** led状态的位图 */
    uint64_t ledType[BITS_TO_UINT64(LED_CNT)];

    /** 声音状态的位图 */
    uint64_t soundType[BITS_TO_UINT64(SND_CNT)];

    /** 开关状态的位图 */
    uint64_t switchType[BITS_TO_UINT64(SW_CNT)];
} InputDevAbility;

/**
 * @brief Input设备的维度信息。
 */
typedef struct {
    /** 坐标轴 */
    int32_t axis;

    /** 记录各个坐标的最小值 */
    int32_t min;

    /** 记录各个坐标的最大值 */
    int32_t max;

    /** 记录各个坐标的分辨率 */
    int32_t fuzz;

    /** 记录各个坐标的基准值 */
    int32_t flat;

    /** 范围 */
    int32_t range;
} InputDimensionInfo;

/**
 * @brief Input设备的识别信息。
 */
typedef struct {
    /** 总线类型 */
    uint16_t busType;

    /** 生产商编号 */
    uint16_t vendor;

    /** 产品编号 */
    uint16_t product;

    /** 版本号 */
    uint16_t version;
} InputDevIdentify;

/**
 * @brief Input设备属性。
 */
typedef struct {
    /** 设备名 */
    char devName[DEV_NAME_LEN];

    /** 设备识别信息 */
    InputDevIdentify id;

    /** 设备维度信息 */
    InputDimensionInfo axisInfo[ABS_CNT];
} InputDevAttr;

/**
 * @brief Input设备基础设备信息。
 */
typedef struct {
    /** 设备索引 */
    uint32_t devIndex;

    /** 设备类型 */
    uint32_t devType;

    /** 驱动芯片编码信息 */
    char chipInfo[CHIP_INFO_LEN];

    /** 模组厂商名 */
    char vendorName[VENDOR_NAME_LEN];

    /** 驱动芯片型号 */
    char chipName[CHIP_NAME_LEN];

    /** 设备属性 */
    InputDevAttr attrSet;

    /** 设备能力属性 */
    InputDevAbility abilitySet;
} InputDeviceInfo;

/**
 * @brief 扩展指令的数据结构。
 */
typedef struct {
    /** 指令对应的编码 */
    const char *cmdCode;

    /** 指令传输的数据 */
    const char *cmdValue;
} InputExtraCmd;

#ifdef __cplusplus
}
#endif
#endif
/** @} */
